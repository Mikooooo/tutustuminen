function kortin_arvo(email, nimi2, aika2, paikka2, pv2, millon2) {
let otsikko = document.getElementById("email");
otsikko.innerHTML = email;
let nimi = document.getElementById("nimi");
nimi.innerHTML = nimi2;
let aika = document.getElementById("aika");
aika.innerHTML = aika2;
let paikka = document.getElementById("paikka");
paikka.innerHTML = paikka2;
let pv = document.getElementById("pv");
pv.innerHTML = pv2;
let millon = document.getElementById("millon");
millon.innerHTML = millon2;
}

function parametrit() {
    const queryString = window.location.search;
    console.log(queryString);
    const urlParams = new URLSearchParams(queryString);
    const email = urlParams.get('email')
    console.log(email);
    const nimi2 = urlParams.get('nimi')
    console.log(nimi2);
    const aika2 = urlParams.get('aika')
    console.log(aika2);
    const paikka2 = urlParams.get('paikka')
    console.log(paikka2);
    const pv2 = urlParams.get('pv')
    console.log(pv2);
    const millon2 = urlParams.get('millon')
    console.log(millon2);
    kortin_arvo(email, nimi2, aika2, paikka2, pv2, millon2);
}
window.onload = parametrit;