// ekat napit
document.addEventListener("DOMContentLoaded", function() {
  const paragraph = document.getElementById("edit");
  const edit_button = document.getElementById("edit-button");
  const end_button = document.getElementById("end-editing");

  edit_button.addEventListener("click", function() {
    paragraph.innerHTML = ""; // poistaa kentässä olleen vanhan tekstin, voi ottaa pois jos haluu :D
    paragraph.contentEditable = true;
    paragraph.style.backgroundColor = "#dddbdb";
  });

  end_button.addEventListener("click", function() {
    paragraph.contentEditable = false;
    paragraph.style.backgroundColor = "#ffffff";
  });
});


// tokat napit
document.addEventListener("DOMContentLoaded", function() {
  const paragraph = document.getElementById("edit2");
  const edit_button = document.getElementById("edit-button2");
  const end_button = document.getElementById("end-editing2");

  edit_button.addEventListener("click", function() {
    paragraph.innerHTML = "";
    paragraph.contentEditable = true;
    paragraph.style.backgroundColor = "#dddbdb";
  });

  end_button.addEventListener("click", function() {
    paragraph.contentEditable = false;
    paragraph.style.backgroundColor = "#ffffff";
  });
});